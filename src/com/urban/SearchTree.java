package com.urban;

import com.urban.ListItem;


public class SearchTree implements NodeList {

    private ListItem root = null;

    public SearchTree(ListItem root) {
        this.root = root;
    }

    @Override
    public ListItem getRoot() {
        return this.root;
    }

    @Override
    public boolean addItem(ListItem newItem) {
        if (this.root == null) {
            // the tree was empty
            this.root = newItem;
            return true;
        }

        // start comparing from the head of the tree
        ListItem currentItem = this.root;
        while (currentItem != null) {
            int comparison = (currentItem.compareTo(newItem));

            // newItem is greater, move right if possible
            if (comparison < 0) {
                if (currentItem.next() != null) {
                    currentItem = currentItem.next();
                // no node to the right, add at this point
                } else {
                    currentItem.setNext(newItem);
                    return true;
                }
            // newItem is less, move left if possible
            } else if (comparison > 0) {
                if (currentItem.previous() != null) {
                    currentItem = currentItem.previous();
                // no node to the left, add at this point
                } else {
                    currentItem.setPrevious(newItem);
                    return true;
                }
            // equal -> do not add
            } else {
                System.out.println(newItem.getValue() + " is already present");
                return false;
            }
        }

        return false;
    }

    @Override
    public boolean removeItem(ListItem item) {
        if (item != null) {
            System.out.println("Deleting item " + item.getValue());
        }

        ListItem currentItem = this.root;
        ListItem parentItem = currentItem;

        while (currentItem != null) {
            int comparison = (currentItem.compareTo(item));

            if (comparison < 0) {
                parentItem = currentItem;
                currentItem = currentItem.next();
            } else if (comparison > 0) {
                parentItem = currentItem;
                currentItem = currentItem.previous();
            } else {
                // found the item, remove it
                performRemoval(currentItem, parentItem);
                return true;
            }
        }
        return false;
    }

    private void performRemoval(ListItem item, ListItem parent) {
        // remove item from the tree
        if (item.next() == null) {
            if (parent.next() == item) {
                parent.setNext(item.previous());
            } else if (parent.previous() == item) {
                parent.setPrevious(item.previous());
            } else {
                this.root = item.previous();
            }
        } else if (item.previous() == null) {
            // no left tree, make parent point to right tree
            if (parent.next() == item) {
                // item is right child of its parent
                parent.setNext(item.next());
            } else if (parent.previous() == item) {
                // item is left child of its parent
                parent.setPrevious(item.next());
            } else {
                this.root = item.next();
            }
        } else {
            // From the right sub-tree, find the smallest value
            ListItem current = item.next();
            ListItem leftmostParent = item;

            while (current.previous() != null) {
                leftmostParent = current;
                current = current.previous();
            }

            // put the smallest value into our node to be deleted
            item.setValue(current.getValue());

            // delete the smallest
            if (leftmostParent == item) {
                item.setNext(current.next());
            } else {
                leftmostParent.setPrevious(current.next());
            }
        }
    }

    @Override
    public void traverse(ListItem root) {
        // recursive method
        if (root != null) {
            traverse(root.previous());
            System.out.println(root.getValue());
            traverse(root.next());
        }

    }
}
